"""
Impelmentation of model one
"""

import numpy as np
from keras.models import Model
from keras.layers import add, dot, multiply, Input
from keras import backend as b

import logpdfs



class VaeZX(object):
    """
        model 1
    """
    prior_sd = 0

    def __init__(self, optimizer, n_x, n_hidden_q, n_z, n_hidden_p, nonlinear_q='tanh',
                 nonlinear_p='tanh', type_px='bernoulli', type_qz='gaussianmarg',
                 type_pz='gaussianmarg', prior_sd=1, init_sd=1e-2, var_smoothing=0, n_mixture=50):
        self.n_x = n_x
        self.n_hidden_q = n_hidden_q
        self.n_z = n_z
        self.n_hidden_p = n_hidden_p
        self.dropout = False
        self.nonlinear_q = nonlinear_q
        self.nonlinear_p = nonlinear_p
        self.type_px = type_px
        self.type_qz = type_qz
        self.type_pz = type_pz
        self.prior_sd = prior_sd
        self.var_smoothing = var_smoothing
        self.n_mixture = n_mixture
        self.init_sd = init_sd

        self.dist_px = {}
        self.weights = {}
        self.hidden_p = {}
        # Helper variables
        self.z = {}
        self.A = {}
        self.x = {}
        self._z = 0
        self.q_mean = 0
        self.q_logvar = 0
        self.v = {}
        self.dist_qz = []

        self.dist_qz = {}
        self.dist_px = {}
        self.dist_pz = {}

        self.logpx, self.logpz, self.logqz = self.__get_factors()

        # p(x|z)
        self.likelihood = (self.logpx + self.logpz - self.logqz).sum()

        self.model = Model(inputs=self.x, outputs=self.x)
        self.model.compile(self, optimizer, self.likelihood)


    def __get_bernoulli_logpx(self):
        #p = T.nnetsigmoid(T.dot(w['out_w'], hidden_p[-1]) + T.dot(w['out_b'], A))
        prediction = b.sigmoid(add((dot(self.weights["out_w"], self.hidden_p[-1]),
                                    dot(self.weights["out_b"], self.A))))

        #_logpx = - T.nnet.binary_crossentropy(p, x['x'])
        _logpx = -1 * b.binary_crossentropy(self.x['x'], prediction, True)

        #dist_px['x'] = theanofunc([_z] + [A], p)
        return _logpx


    def __get_gaussian_logpx(self):
        #x_mean = T.dot(w['out_w'], hidden_p[-1]) + T.dot(w['out_b'], A)
        x_mean = add((dot(self.weights['out_w'], self.hidden_p[-1]),
                      dot(self.weights['out_b'], self.A)))

        #x_logvar = T.dot(w['out_logvar_w'], hidden_p[-1]) + T.dot(w['out_logvar_b'], A)
        x_logvar = add((dot(self.weights['out_logvar_w'], self.hidden_p[-1]),
                        dot(self.weights['out_logvar_b'], self.A)))

        #_logpx = ap.logpdfs.normal2(x['x'], x_mean, x_logvar)
        _logpx = logpdfs.normal2(self.x['x'], x_mean, x_logvar)

        #dist_px['x'] = theanofunc([_z] + [A], [x_mean, x_logvar])
        return _logpx

    def __get_bounded01_logpx(self):
        #x_mean = T.nnet.sigmoid(T.dot(w['out_w'], hidden_p[-1]) + T.dot(w['out_b'], A))
        x_mean = b.sigmoid(add((dot(self.weights['out_w'], self.hidden_p[-1]),
                                dot(self.weights['out_b'], self.A))))

        #x_logvar = T.dot(w['out_logvar_b'], A)
        x_logvar = dot(self.weights['out_logvar_b'], self.A)

        #_logpx = ap.logpdfs.normal2(x['x'], x_mean, x_logvar)
        _logpx = logpdfs.normal2(self.x['x'], x_mean, x_logvar)

        # Make it a mixture between uniform and Gaussian
        #w_unif = T.nnet.sigmoid(T.dot(w['out_unif'], A))
        w_unif = b.sigmoid(b.dot(self.weights['out_unif'], self.A))

        #_logpx = T.log(w_unif + (1-w_unif) * T.exp(_logpx))
        _logpx = b.log(w_unif + (1-w_unif) * b.exp(_logpx))
        #dist_px['x'] = theanofunc([_z] + [A], [x_mean, x_logvar])

    def __get_logpx(self):
        """
        logpx = log p(x|z,w)
        """
        if self.type_px is "bernoulli":
            _logpx = self.__get_bernoulli_logpx()
        elif self.type_px is 'gaussian':
            _logpx = self.__get_gaussian_logpx()
        elif self.type_px is 'bounded01':
            _logpx = self.__get_bernoulli_logpx()

        #logpx = T.dot(shared32(np.ones((1, self.n_x))), _logpx)
        logpx = dot(np.ones((1, self.n_x)), _logpx)
        return logpx


    def __get_logpz(self):
        """
        log p(z) (prior of z)
        """
        if self.type_pz is 'gaussianmarg':
            logpz = -0.5 * (np.math.log(2 * np.pi) + (self.q_mean**2 + b.exp(self.q_logvar)))
            logpz = logpz.sum(axis=0, keepdims=True)
        elif self.type_pz is 'gaussian':
            logpz = logpdfs.standard_normal(self._z).sum(axis=0, keepdims=True)
        elif self.type_pz is 'mog':
            prior_z = 0
            for i in range(self.n_mixture):
                prior_z += b.exp(logpdfs.normal2(self._z,
                                                 b.dot(self.weights['mog_mean'+str(i)], self.A),
                                                 b.dot(self.weights['mog_logvar'+str(i)], self.A)))

            logpz = b.log(prior_z).sum(axis=0, keepdims=True)
            logpz = logpz - self.n_z * np.math.log(float(self.n_mixture))
        elif self.type_pz is 'laplace':
            logpz = logpdfs.standard_laplace(self._z).sum(axis=0, keepdims=True)
        elif self.type_pz is 'studentt':
            logpz = logpdfs.studentt(self._z, b.dot(b.exp(self.weights['logv']), self.A))
            logpz = logpz.sum(axis=0, keepdims=True)

        return logpz

    def __get_logqz(self, logpx, logpz):
        """
        loq q(z|x) (entropy of z)
        """
        if self.type_qz == 'gaussianmarg':
            logqz = - 0.5 * (np.math.log(2 * np.pi) + 1 + self.q_logvar).sum(axis=0, keepdims=True)
        elif self.type_qz == 'gaussian':
            logqz = logpdfs.normal2(self._z, self.q_mean, self.q_logvar).sum(axis=0, keepdims=True)
        else: raise Exception()

        # [new part] Fisher divergence of latent variables
        if self.var_smoothing > 0:
            # gives error when using gaussianmarg instead of gaussian
            #dlogq_dz = T.grad(logqz.sum(), _z)
            #dlogp_dz = T.grad((logpx + logpz).sum(), _z)
            #FD = 0.5 * ((dlogq_dz - dlogp_dz)**2).sum(axis=0, keepdims=True)
            #[end new part]
            #logqz -= self.var_smoothing * FD

            dlogq_dz = b.gradients(logqz.sum(), self._z)
            dlogp_dz = b.gradients((logpx + logpz).sum(), self._z)
            fisher_divergence = 0.5 * ((dlogq_dz - dlogp_dz)**2).sum(axis=0, keepdims=True)
            logqz -= self.var_smoothing * fisher_divergence
        return logpz

    @classmethod
    def __f_softplus(cls, x_values):
        return b.log(b.exp(x_values) + 1)# - np.log(2)

    @classmethod
    def __f_rectlin(cls, x_values):
        return x_values * (x_values > 0)

    @classmethod
    def __f_rectlin2(cls, x_values):
        return x_values * (x_values > 0) + 0.01 * x_values

    def __f_prior(self, _w, prior_sd=prior_sd):
        return logpdfs.normal(_w, 0, prior_sd).sum()

    def __get_factors(self):
        '''
        z['eps'] is the independent epsilons (Gaussian with unit variance)
        x['x'] is the data

        The names of dict z[...] may be confusing here: the latent variable z is not included
        in the dict z[...], but implicitely computed from epsilon and parameters in w.

        z is computed with g(.) from eps and variational parameters
        let logpx be the generative model density: log p(x|z) where z=g(.)
        let logpz be the prior of Z plus the entropy of q(z|x): logp(z) + H_q(z|x)
        So the lower bound L(x) = logpx + logpz

        let logpv and logpw be the (prior) density of the parameters
        '''


        # Compute q(z|x)
        # Q(z|X) = N(z|µ(X;ϑ),Σ(X;ϑ))
        # µ and Σ are arbitrary deterministic functions with parameters ϑ that can be learned
        # µ and Σ are implemented via neural networks, and Σ is constrained to be a diagonal matrix

        #input layer
        hidden_q = [self.x['x']]

        nonlinear = {'tanh': b.tanh,
                     'sigmoid': b.sigmoid,
                     'softplus': self.__f_softplus,
                     'rectlin': self.__f_rectlin,
                     'rectlin2': self.__f_rectlin2}
        nonlinear_q_func = nonlinear[self.nonlinear_q]
        nonlinear_p_func = nonlinear[self.nonlinear_p]

        # begin encoder
        # for each layer connect to previous layer
        for i in range(len(self.n_hidden_q)):
            hidden_q.append(nonlinear_q_func(add((dot(self.v['w'+str(i)], hidden_q[-1]),
                                                  dot(self.v['b'+str(i)], self.A)))))
            if self.dropout:
                hidden_q[-1] *= 2. * (b.random_uniform(size=hidden_q[-1].shape, seed=0) > .5)
        #end encoder


        q_mean = add((dot(self.v['mean_w'], hidden_q[-1]),
                      dot(self.v['mean_b'], self.A)))

        if self.type_qz == 'gaussian' or self.type_qz == 'gaussianmarg':
            q_logvar = add((dot(self.v['logvar_w'], hidden_q[-1]),
                            dot(self.v['logvar_b'], self.A)))
        else: raise Exception()


        # Compute virtual sample
        _z = add((q_mean, multiply((b.exp(multiply((0.5, q_logvar))), self.z['eps']))))

        # Compute log p(x|z)
        hidden_p = [_z]

        #begin decoder
        for i in range(len(self.n_hidden_p)):
            #hidden_p.append(nonlinear_p(T.dot(w['w'+str(i)], hidden_p[-1]) +
            #                            T.dot(w['b'+str(i)], A)))
            hidden_p.append(nonlinear_p_func(add((dot(self.weights['w'+str(i)], hidden_p[-1]),
                                                  dot(self.weights['b'+str(i)], self.A)))))

            if self.dropout:
                #hidden_p[-1] *= 2. * (rng.uniform(size=hidden_p[-1].shape, dtype='float32') > .5)
                hidden_p[-1] *= 2. * (b.random_uniform(size=hidden_p[-1].shape, seed=0) > .5)
        #end decoder


        logpx = self.__get_logpx()
        logpz = self.__get_logpz()
        logqz = self.__get_logqz(logpx, logpz)

        return logpx, logpz, logqz


    def train_on_batch(self, x_data, y_data):
        """
        Train the model on the given data.

        Returns:
            Training Loss
        """
        return self.model.train_on_batch(self, x_data, y_data)

    def evaluate(self, x_data, y_data):
        """
        Evaluate the model on the given data.

        Returns:
            Evalluation error
        """
        return self.model.evaluate(x_data, y_data)




    # Compute likelihood lower bound given a variational auto-encoder
    # L is number of samples
    # train
    def est_loglik(self, x, n_batch, n_samples=1, byteToFloat=False):
        
        #1.)  get mini batch (D <- getRandomMiniBatch())
        n_tot = x.itervalues().next().shape[1]
        
        px = 0 # estimate of marginal likelihood
        lowbound = 0 # estimate of lower bound of marginal likelihood
        for _ in range(n_samples):
            _L = np.zeros((1,0))
            i = 0
            while i < n_tot:
                i_to = min(n_tot, i+n_batch)
                _x = ndict.getCols(x, i, i_to)
                if byteToFloat: _x = {i:_x[i].astype(np.float32)/256. for i in _x}

                _L = np.hstack((_L, self.eval(_x, {})))
                i += n_batch
            lowbound += _L.mean()
            px += np.exp(_L)
        
        lowbound /= n_samples
        logpx = np.log(px / n_samples).mean()
        return lowbound, logpx
        
    # A = np.ones((1, n_batch))
    # def get_A(self, x): return np.ones((1, x.itervalues().next().shape[1])).astype('float32')

    def save(self):
        # serialize model to JSON
        model_json = self.model.to_json()
        with open("model.json", "w") as json_file:
            json_file.write(model_json)
        # serialize weights to HDF5
        self.model.save_weights("model.h5")
        print("Saved model to disk")


    def load(self):
        # load json and create model
        json_file = open('model.json', 'r')
        loaded_model_json = json_file.read()
        json_file.close()
        loaded_model = model_from_json(loaded_model_json)
        # load weights into new model
        loaded_model.load_weights("model.h5")
        loaded_model.compile(loss='binary_crossentropy', optimizer='rmsprop', metrics=['accuracy'])
        print("Loaded model from disk")