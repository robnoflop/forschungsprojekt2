import vea_z_x
from keras.datasets import mnist
from keras.optimizers import Adam

# MNIST
size = 28
#train_x, train_y, valid_x, valid_y, test_x, test_y = mnist.load_numpy(size)
(train_x, train_y), (test_x, test_y) = mnist.load_data()
valid_x = train_x[0:10000]
valid_y = train_y[0:10000]
train_x = train_x[10000:]
train_y = train_y[10000:] 

x = {'x': train_x.astype(np.float32)}
x_valid = {'x': valid_x.astype(np.float32)}
x_test = {'x': test_x.astype(np.float32)}
L_valid = 1
dim_input = (size,size)
n_x = size*size
type_qz = 'gaussianmarg'
type_pz = 'gaussianmarg'
nonlinear = 'softplus'
type_px = 'bernoulli'
n_train = 50000
n_batch = 1000
colorImg = False
bernoulli_x = True
byteToFloat = False
weight_decay = float(n_batch)/n_train


# Some statistics for optimization
ll_valid_stats = [-1e99, 0]

learning_rate=3e-4
decay1=0.1
decay2=0.001
weight_decay=0.0


optimizer = Adam(learning_rate, decay1, decay2, 1e-08, weight_decay)
model = VAE_Z_X(optimizer, n_x, n_hidden, n_z, n_hidden[::-1], nonlinear, nonlinear, type_px,
                type_qz=type_qz, type_pz=type_pz, prior_sd=100, init_sd=1e-3)

run_traininge_loop(model, hook)

# Training loop for variational autoencoder
def run_traininge_loop(model, hook, n_epochs=9999999):
    training_start_time = time.time()
    for epoch in xrange(1, n_epochs):
        L = model.train_on_batch(train_x, {})
        
        hook(model, epoch, time.time() - training_start_time, L)

    print('Optimization loop finished')

# Progress hook
def hook(model, epoch, training_time, loss):

    if epoch%10 != 0: return

    # Compute likelihood lower bound given a variational auto-encoder
    ll_valid, _ = model.est_loglik(x_valid,
                                   n_samples=L_valid,
                                   n_batch=n_batch,
                                   byteToFloat=byteToFloat)
    # logging and save best model

    # Log 
    # ndict.savez(ndict.get_value(model.v), logdir+'v')
    # ndict.savez(ndict.get_value(model.w), logdir+'w')

    # if ll_valid > ll_valid_stats[0]:
    #    ll_valid_stats[0] = ll_valid
    #    ll_valid_stats[1] = 0
    #    ndict.savez(ndict.get_value(model.v), logdir+'v_best')
    #    ndict.savez(ndict.get_value(model.w), logdir+'w_best')
    #else:
    #    ll_valid_stats[1] += 1
    #    # Stop when not improving validation set performance in 100 iterations
    #    if ll_valid_stats[1] > 1000:
    #        print("Finished")
    #        with open(logdir+'hook.txt', 'a') as f:
    #            print >>f, "Finished"
    #        exit()

    print(epoch, t, ll, ll_valid, ll_valid_stats)
    with open(logdir+'hook.txt', 'a') as f:
        print(f, epoch, t, ll, ll_valid, ll_valid_stats)

    # Graphics
    if gfx and epoch%gfx_freq == 0:

        #tail = '.png'
        tail = '-'+str(epoch)+'.png'

        v = {i: model.v[i].get_value() for i in model.v}
        w = {i: model.w[i].get_value() for i in model.w}

        if 'pca' not in dataset and 'random' not in dataset and 'normalized' not in dataset:

            if 'w0' in v:
                image = paramgraphics.mat_to_img(f_dec(v['w0'][:].T), dim_input, True, colorImg=colorImg)
                image.save(logdir+'q_w0'+tail, 'PNG')

            image = paramgraphics.mat_to_img(f_dec(w['out_w'][:]), dim_input, True, colorImg=colorImg)
            image.save(logdir+'out_w'+tail, 'PNG')

            if 'out_unif' in w:
                image = paramgraphics.mat_to_img(f_dec(w['out_unif'].reshape((-1,1))), dim_input, True, colorImg=colorImg)
                image.save(logdir+'out_unif'+tail, 'PNG')

            if n_z == 2:
                n_width = 10
                import scipy.stats
                z = {'z':np.zeros((2,n_width**2))}
                for i in range(0,n_width):
                    for j in range(0,n_width):
                        z['z'][0,n_width*i+j] = scipy.stats.norm.ppf(float(i)/n_width+0.5/n_width)
                        z['z'][1,n_width*i+j] = scipy.stats.norm.ppf(float(j)/n_width+0.5/n_width)
                
                x, _, _z = model.gen_xz({}, z, n_width**2)
                if dataset == 'mnist':
                    x = 1 - _z['x']
                image = paramgraphics.mat_to_img(f_dec(_z['x']), dim_input)
                image.save(logdir+'2dmanifold'+tail, 'PNG')
            else:
                _x, _, _z_confab = model.gen_xz({}, {}, n_batch=144)
                x_samples = _z_confab['x']
                image = paramgraphics.mat_to_img(f_dec(x_samples), dim_input, colorImg=colorImg)
                image.save(logdir+'samples'+tail, 'PNG')
                
                #x_samples = _x['x']
                #image = paramgraphics.mat_to_img(x_samples, dim_input, colorImg=colorImg)
                #image.save(logdir+'samples2'+tail, 'PNG')
                
        else:
            # Model with preprocessing
            
            if 'w0' in v:
                image = paramgraphics.mat_to_img(f_dec(v['w0'][:].T), dim_input, True, colorImg=colorImg)
                image.save(logdir+'q_w0'+tail, 'PNG')
                
            image = paramgraphics.mat_to_img(f_dec(w['out_w'][:]), dim_input, True, colorImg=colorImg)
            image.save(logdir+'out_w'+tail, 'PNG')

            _x, _, _z_confab = model.gen_xz({}, {}, n_batch=144)
            x_samples = f_dec(_z_confab['x'])
            x_samples = np.minimum(np.maximum(x_samples, 0), 1)
            image = paramgraphics.mat_to_img(x_samples, dim_input, colorImg=colorImg)
            image.save(logdir+'samples'+tail, 'PNG')
            
            
            
# Optimize
#SFO

# Learning step for variational auto-encoder
# def epoch_vae_adam(model, x, n_batch=100, convertImgs=False, bernoulli_x=False, byteToFloat=False):
#     print('Variational Auto-Encoder', n_batch)
    
#     def doEpoch():
        
#         #from collections import OrderedDict

#         n_tot = x.itervalues().next().shape[1]
#         idx_from = 0
#         L = 0
#         while idx_from < n_tot:
#             idx_to = min(n_tot, idx_from+n_batch)
#             x_minibatch = ndict.getCols(x, idx_from, idx_to)
#             idx_from += n_batch
#             if byteToFloat: x_minibatch['x'] = x_minibatch['x'].astype(np.float32)/256.
#             if bernoulli_x: x_minibatch['x'] = np.random.binomial(n=1, p=x_minibatch['x']).astype(np.float32)
            
#             # Do gradient ascent step
#             L += model.evalAndUpdate(x_minibatch, {}).sum()
#             #model.profmode.print_summary()
            
#         L /= n_tot
        
#         return L
        
#     return doEpoch


# def get_adam_optimizer(learning_rate=0.001, decay1=0.1, decay2=0.001, weight_decay=0.0):
#     print('AdaM', learning_rate, decay1, decay2, weight_decay)
#     return Adam(learning_rate, decay1, decay2, epsilon=1e-08, weight_decay)

    # def shared32(x, name=None, borrow=False):
    #     return theano.shared(np.asarray(x, dtype='float32'), name=name, borrow=borrow)

    # def get_optimizer(w, g):
        # updates = OrderedDict()
        
        # it = shared32(0.)
        # updates[it] = it + 1.
        
        # fix1 = 1.-(1.-decay1)**(it+1.) # To make estimates unbiased
        # fix2 = 1.-(1.-decay2)**(it+1.) # To make estimates unbiased
        # lr_t = learning_rate * T.sqrt(fix2) / fix1
        
        # for i in w:
    
        #     gi = g[i]
        #     if weight_decay > 0:
        #         gi -= weight_decay * w[i] #T.tanh(w[i])

        #     # mean_squared_grad := E[g^2]_{t-1}
        #     mom1 = shared32(w[i].get_value() * 0.)
        #     mom2 = shared32(w[i].get_value() * 0.)
            
        #     # Update moments
        #     mom1_new = mom1 + decay1 * (gi - mom1)
        #     mom2_new = mom2 + decay2 * (T.sqr(gi) - mom2)
            
        #     # Compute the effective gradient and effective learning rate
        #     effgrad = mom1_new / (T.sqrt(mom2_new) + 1e-10)
            
        #     effstep_new = lr_t * effgrad
            
        #     # Do update
        #     w_new = w[i] + effstep_new
                
        #     # Apply update
        #     updates[w[i]] = w_new
        #     updates[mom1] = mom1_new
        #     updates[mom2] = mom2_new
            
        # return updates
        
    # return get_optimizer
    