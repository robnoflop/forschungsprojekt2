"""
Autor: Robert Kasseck
"""

from keras.layers import multiply, Input, Dense, Lambda, concatenate
import keras.backend as K
from keras import metrics
from keras import optimizers
from keras.models import Model
from distributions import Distributions
import probability_density_functions as pdf
import numpy as np
from keras.regularizers import l2
from keras.activations import softplus
import tensorflow as tf
from model1 import VariationalAutoEncoder1
from model2 import VariationalAutoEncoder2

class VariationalAutoEncoder12():
    """
    This class is a base class for an vaiational auto encoder
    """


    def __init__(self,
                 input_dim,
                 output_dim,
                 num_classes,
                 batch_size,
                 encoder_layer_sizes1,
                 latent_dimension1,
                 decoder_layer_sizes1,
                 encoder_layer_sizes2,
                 latent_dimension2,
                 decoder_layer_sizes2,
                 encoder_activation_func='relu',
                 decoder_activation_func='relu',
                 generation_distribution=Distributions.BERNOULLI,
                 prior_z_distribution=Distributions.GAUSSIANMARG,
                 type_qz=Distributions.GAUSSIANMARG,
                 weight_decay = 0.,
                 var_smoothing=0,
                 seed = 0):
        

        self.model1 = VariationalAutoEncoder1(input_dim = input_dim,
                                              output_dim = output_dim,
                                              batch_size = batch_size,
                                              encoder_layer_sizes = encoder_layer_sizes1,
                                              latent_dimension = latent_dimension1,
                                              decoder_layer_sizes = decoder_layer_sizes1,
                                              # original
                                              encoder_activation_func='softplus',
                                              decoder_activation_func='softplus',
                                              #encoder_activation_func='relu',
                                              #decoder_activation_func='relu',
                                              generation_distribution=Distributions.BERNOULLI, #type_px
                                              prior_z_distribution=Distributions.GAUSSIANMARG, #type_pz
                                              type_qz=Distributions.GAUSSIANMARG, 
                                              weight_decay = weight_decay)

        self.model2 = VariationalAutoEncoder2(input_dim = latent_dimension1,
                                              output_dim = latent_dimension1,
                                              num_classes = 10,
                                              batch_size = batch_size,
                                              encoder_layer_sizes = encoder_layer_sizes2,
                                              latent_dimension = latent_dimension2,
                                              decoder_layer_sizes = decoder_layer_sizes2
                                              # original
                                              #encoder_activation_func='softplus',
                                              #decoder_activation_func='softplus',
                                              encoder_activation_func='relu',
                                              decoder_activation_func='relu',
                                              generation_distribution=Distributions.BERNOULLI, #type_px
                                              prior_z_distribution=Distributions.GAUSSIANMARG, #type_pz
                                              type_qz=Distributions.GAUSSIANMARG, 
                                              weight_decay = weight_decay)

        

    def fit(self, x_input, y_output, y, valid_x_input, valid_y_output, valid_y, epochs = 9999999):
        self.model1.fit(x_input, y_output, valid_x_input, valid_y_output, epochs)
        
        train_z1 = self.model1.generateZ(x_input)
        valid_z1 = self.model1.generateZ(valid_x_input)

        self.model2.fit(train_z1, train_z1, y, valid_z1, valid_z1, valid_y, epochs)

        


    def myGenerator(self, x, y, x_y):
        from random import shuffle
        position = 0
        while 1:
            print("generating")
            exit()
            x_ret = x[position:position + self.batch_size]
            y_ret = y[position:position + self.batch_size]
            x_y_ret = x_y[position:position + self.batch_size]
            yield {'x_input': x_ret, 'y_input': y_ret}, x_y_ret
            position += self.batch_size
            if position + self.batch_size > len(x):
                position = 0
                x_shuf = []
                y_shuf = []
                x_y_shuf = []
                index_shuf = list(range(len(x)))
                shuffle(index_shuf)
                for i in index_shuf:
                    x_shuf.append(x[i])
                    y_shuf.append(y[i])
                    x_y_shuf.append(x_y[i])

                x = np.asarray(x_shuf)
                y = np.asarray(y_shuf)
                x_y = np.asarray(x_y_shuf)

    