from keras.datasets import mnist
import sys
from keras.layers import multiply, Input, Dense, Lambda
import keras.backend as K
from keras import metrics
from keras import optimizers
from keras.models import Model
from distributions import Distributions
import probability_density_functions as pdf
import numpy as np
from keras.regularizers import l2
import tensorflow as tf


def model_dense_layer(layer_size, layer_activation_func, name=None):
    return Dense(layer_size,
                    activation=layer_activation_func,
                    #ToDo: should be np.random.normal(0, 1, size=size) / np.sqrt(size[1])
                    #kernel_initializer='random_normal',
                    kernel_initializer= valriable_initialization,
                    bias_initializer=bias_initialization,
                    name=name)

def bias_initialization(shape, name=None):
    return K.variable(np.random.normal(0, 1e-2, shape), name=name)

def valriable_initialization(shape, name=None):
    # value = np.random.normal(0, 1, size=shape) / np.sqrt(shape[1])
    var = K.variable(np.random.uniform(0.0, 1.0, shape) / np.sqrt(shape[1]), name=name)
    #var = K.print_tensor(var)
    return var


def calc_loss(x, decoder_mean): #decoder, q_log_var, q_mean, z):
    """
    Calculate the loss.

    Retruns:
        The loss value.
    """
    xent_loss = metrics.binary_crossentropy(x, decoder_mean)
    #kl_loss = - 0.5 * K.sum(1 + z_log_var - K.square(z_mean) - K.exp(z_log_var), axis=-1)
    kl_loss = - 0.5  * K.sum(1 + q_log_var - K.square(q_mean) - K.exp(q_log_var), axis=-1)
    #kl_loss = tf.Print(kl_loss, [kl_loss])
    #kl_loss = K.print_tensor(kl_loss, message='')
    return xent_loss + kl_loss


(x_train, y_train), (x_test, y_test) = mnist.load_data()

layers = [500, 500]
#x_train = x_train.astype('float32') / 255.
#x_test = x_test.astype('float32') / 255.
x_train = x_train.reshape((len(x_train), np.prod(x_train.shape[1:])))
x_test = x_test.reshape((len(x_test), np.prod(x_test.shape[1:])))

batch_size = 100
input_dim = x_train[0].shape[0]
output_dim = x_train[0].shape[0]
encoder_layer_sizes = layers
latent_dimension = 50
decoder_layer_sizes = layers[::-1]
encoder_activation_func ='softplus'
decoder_activation_func ='softplus'
weight_decay = batch_size/50000

x_input = Input(batch_shape=(batch_size, input_dim))

encoder = model_dense_layer(encoder_layer_sizes[0],
                            encoder_activation_func,
                            name="encoder_layer_0")(x_input)

for i in range(1, len(encoder_layer_sizes)):
    encoder = model_dense_layer(encoder_layer_sizes[i],
                                encoder_activation_func,
                                name="encoder_layer_"+str(i))(encoder)

q_mean = Dense(latent_dimension)(encoder)
q_log_var = Dense(latent_dimension)(encoder)

def sampling(args):
    q_mean, q_log_var = args
    epsilon = K.random_normal(shape=(batch_size, latent_dimension),
                                            mean=0.,
                                            stddev=1.0)
    return q_mean + K.exp(q_log_var / 2) * epsilon

z = Lambda(sampling, output_shape=(latent_dimension,))([q_mean, q_log_var])

decoder = model_dense_layer(decoder_layer_sizes[0],
                            decoder_activation_func,
                            name="decoder_layer_0")(z)

for i in range(1, len(decoder_layer_sizes)):
    decoder = model_dense_layer(decoder_layer_sizes[i],
                                decoder_activation_func,
                                name="decoder_layer_"+str(i))(decoder)

decoded_mean = Dense(output_dim, name='decoder_mean')(decoder)

model = Model(x_input, decoded_mean)
optimizer = optimizers.adam(0.001, 0.1, 0.001, decay=weight_decay)
model.compile(optimizer=optimizer, loss=calc_loss)

import keras
tbCallBack = keras.callbacks.TensorBoard(log_dir='./Graph', histogram_freq=1, batch_size=batch_size, write_graph=True, write_images=True)
model.fit(x_train, x_train, 
          shuffle=True,
          epochs=10,
          batch_size=batch_size,
          validation_data=(x_test, y_test),
          callbacks=[tbCallBack])
