"""
Autor: Robert Kasseck
"""

from keras.layers import multiply, Input, Dense, Lambda
import keras.backend as K
from keras import metrics
from keras import optimizers
from keras.models import Model
from keras.callbacks import TensorBoard, ModelCheckpoint
from . import probability_density_functions as pdf
import numpy as np
from keras.regularizers import l2
import tensorflow as tf
import os



class VariationalAutoEncoder1():
    """
    This class is a base class for an vaiational auto encoder
    """


    def __init__(self,
                 input_dim,
                 output_dim,
                 batch_size,
                 encoder_layer_sizes,
                 latent_dimension,
                 decoder_layer_sizes,
                 encoder_activation_func='relu',
                 decoder_activation_func='relu',
                 output_format_distribution = "gaussian",
                 decay = 0.,
                 var_smoothing=0,
                 seed = 0,
                 learning_rate = 0.001):

        
        self.input_dim = input_dim
        self.output_dim = output_dim
        self.batch_size = batch_size
        self.x_input = Input(shape=(self.input_dim,))
        
        self.encoder_layer_sizes = encoder_layer_sizes
        self.latent_dimension = latent_dimension #n_z
        self.decoder_layer_sizes = decoder_layer_sizes
        
        self.encoder_activation_func = encoder_activation_func
        self.decoder_activation_func = decoder_activation_func
        self.output_format_distribution = output_format_distribution
        self.var_smoothing = var_smoothing

        print("input_dim: ", input_dim)
        print("output_dim: ", output_dim)
        print("batch_size: ", batch_size)
        print("x_input: ", self.x_input)
        print("encoder_layer_sizes: ", encoder_layer_sizes)
        print("latent_dimension: ", latent_dimension)
        print("decoder_layer_sizes: ", decoder_layer_sizes)
        print("encoder_activation_func: ", encoder_activation_func)
        print("decoder_activation_func: ", decoder_activation_func)
        print("output_format_distribution: ", output_format_distribution)
        print("var_smoothing: ", var_smoothing)
        print("learning_rate: ", learning_rate)


        self.encoder_layer = self.encoder(self.x_input)
        self.z, self.q_log_var, self.q_mean = self.comput_latent_variable(self.encoder_layer)
        
        self.decoder_output_layer = self.decoder(self.z)

        if self.output_format_distribution is "bernulli":
            self.decoder_mean = Dense(self.output_dim, activation='sigmoid', name='decoder_mean')
            decoded_mean = self.decoder_mean(self.decoder_output_layer)
            self.model = Model(self.x_input, decoded_mean)
        elif self.output_format_distribution is "gaussian":
            self.decoder_mean = Dense(self.output_dim, name='decoder_mean')
            self.decoder_logvar = Dense(self.output_dim, name='decoder_logvar')
            decoded_mean = self.decoder_mean(self.decoder_output_layer)
            self.decoder_logvar_output = self.decoder_logvar(self.decoder_output_layer)
            self.model = Model(self.x_input, decoded_mean)

        # orginal settings
        # optimizer = optimizers.adam(0.0005, 0.1, 0.001, decay=decay)
        # self.model.compile(optimizer=optimizer, loss=self.calc_loss)

        optimizer = optimizers.rmsprop(learning_rate, decay=decay)
        self.model.compile(optimizer=optimizer, loss=self.calc_loss)
        self.model.summary()

    def fit(self, x, y, x_val = None, y_val = None, epochs = 9999999, outputdir="", snapshot_file = None):
        start_epoch = 0
        if snapshot_file is not None:
            print("loading weights: ", snapshot_file)
            self.model.load_weights(snapshot_file)
            start_epoch = int(snapshot_file.split("-")[2]) + 1
        
        print("start_epoch: ", start_epoch)
        print("x shape: ", x.shape)
        print("y shape: ", y.shape)
        print("x_val shape: ", x_val.shape)
        print("y_val shape: ", y_val.shape)
        print("epochs: ", epochs)
        print("outputdir: ", outputdir)

        tbCallBack = TensorBoard(log_dir=outputdir, histogram_freq=0, write_graph=True, write_images=True, write_grads=True)

        filepath = os.path.join(outputdir, 'weights-improvement-{epoch:02d}-{val_loss:.2f}.h5')
        checkpoint = ModelCheckpoint(filepath, monitor='val_loss', verbose=1, save_weights_only=True, period=100)

        if x_val is not None:
            self.model.fit_generator(self.myGenerator(x, y),
                                     steps_per_epoch=len(x)/self.batch_size,
                                     epochs=epochs,
                                     callbacks=[tbCallBack, checkpoint],
                                     validation_data=self.myGenerator(x_val, y_val),
                                     validation_steps=len(x_val)/self.batch_size,
                                     initial_epoch=start_epoch)
        else:
            self.model.fit(x, y, 
                        shuffle=True,
                        epochs=epochs,
                        batch_size=self.batch_size,
                        callbacks=[tbCallBack, checkpoint],
                        initial_epoch=start_epoch)

        self.model.save_weights(os.path.join(outputdir, 'final_weights_.h5'))

    def myGenerator(self, x, y):
        
        print("Generator input")
        print("x shape: ", x.shape)
        print("y shape: ", y.shape)


        from random import shuffle
        position = 0
        while 1:
            x_ret = x[position:position + self.batch_size]
            y_ret = y[position:position + self.batch_size]
            yield x_ret, y_ret
            position += self.batch_size
            if position + self.batch_size > len(x):
                position = 0
                x_shuf = []
                y_shuf = []
                index_shuf = list(range(len(x)))
                shuffle(index_shuf)
                for i in index_shuf:
                    x_shuf.append(x[i])
                    y_shuf.append(y[i])

                x = np.asarray(x_shuf)
                y = np.asarray(y_shuf)




    def test(self, train_x, train_y, test_x_unlabled, test_y, samples=100, snapshot_file=None):
        if snapshot_file is not None:
            print("loading weights: ", snapshot_file)
            self.model.load_weights(snapshot_file)
     
        encoder = Model(self.x_input, self.z)
        x_encoded = []
        for i in range(0, train_x.shape[0], self.batch_size):
            x_encoded.extend(encoder.predict_on_batch(train_x[i:i+self.batch_size])) #, batch_size = self.batch_size)

        x_encoded_unlabled = []
        for i in range(0, test_x_unlabled.shape[0], self.batch_size): 
            x_encoded_unlabled.extend(encoder.predict_on_batch(test_x_unlabled[i:i+self.batch_size]))
                                                    #, batch_size = self.batch_size)

        from sklearn.semi_supervised import LabelSpreading, LabelPropagation

        x = np.concatenate([np.asarray(x_encoded), np.asarray(x_encoded_unlabled)])
        y = np.full(len(x), -1)
        for i in range(0, len(train_y)):
            y[i] = train_y[i]
            
        model = LabelPropagation(n_jobs=-1, max_iter=samples, kernel='knn')
        
        model.fit(x, y)
        print(model.score(np.asarray(x_encoded_unlabled), test_y))





    def generate(self, org_image_size, x, y=None, generate_from_x=False, snapshot_file=None):
        if snapshot_file is not None:
            print("loading weights: ", snapshot_file)
            self.model.load_weights(snapshot_file)


        # build a model to project inputs on the latent space
        encoder = Model(self.x_input, self.q_mean)

        # build a digit generator that can sample from the learned distribution
        decoder_input = Input(shape=(self.latent_dimension,))
        decoder = self.decoder_layer[0]
        decoder = decoder(decoder_input)
        for i in range(1, len(self.decoder_layer)):
            layer = self.decoder_layer[i]
            decoder = layer(decoder)


        _x_decoded_mean = self.decoder_mean(decoder)
        generator = Model(decoder_input, _x_decoded_mean)

        import matplotlib.pyplot as plt
        if y is not None:
            # display a 2D plot of the digit classes in the latent space
            x_encoded = encoder.predict(x, batch_size=self.batch_size)
            plt.figure(figsize=(6, 6))
            plt.scatter(x_encoded[:, 0], x_encoded[:, 1], c=y)
            plt.colorbar()
            plt.show()

        if generate_from_x:
            print("start generateing")
            for i in range(len(x)):
                z = encoder.predict(x[i].reshape(1, x[i].shape[0]))

                def reshape(image):
                    if len(org_image_size) == 3:
                        return image.reshape(org_image_size[0], org_image_size[1], org_image_size[2])
                    elif len(org_image_size) == 2:
                        return image.reshape(org_image_size[0], org_image_size[1])

                def save(image, name):
                    from PIL import Image
                    if len(org_image_size) == 3:
                        im = Image.fromarray(np.uint8(image*255))
                    elif len(org_image_size) == 2:
                        im = Image.fromarray(image * 255).convert('L')

                    im.save("implementation/m1out/" + name + ".png")
                    
                digit = generator.predict(z)
                digit = reshape(digit)
                save(digit, str(i))

                digit = generator.predict(z + 0.25)
                digit = reshape(digit)
                save(digit, str(i) + "_p0.25")

                digit = generator.predict(z - 0.25)
                digit = reshape(digit)
                save(digit, str(i) + "_m0.25")

                org = reshape(x[i])
                save(org, str(i) + "_org")

        else: 
            # display a 2D manifold of the digits
            n = 15  # figure with 15x15 digits
            if len(org_image_size) == 3:
                figure = np.zeros((org_image_size[0] * n, org_image_size[1] * n, org_image_size[2]))
            elif len(org_image_size) == 2:
                figure = np.zeros((org_image_size[0] * n, org_image_size[1] * n))
            # linearly spaced coordinates on the unit square were transformed through the inverse CDF (ppf) of the Gaussian
            # to produce values of the latent variables z, since the prior of the latent space is Gaussian
            from scipy.stats import norm
            grid_x = norm.ppf(np.linspace(0.05, 0.95, n))
            grid_y = norm.ppf(np.linspace(0.05, 0.95, n))

            for i, yi in enumerate(grid_x):
                for j, xi in enumerate(grid_y):
                    z_sample = np.array([[xi, yi]])
                    x_decoded = generator.predict(z_sample)
                    if len(org_image_size) == 3:
                        digit = x_decoded[0].reshape(org_image_size[0], org_image_size[1], org_image_size[2])
                    elif len(org_image_size) == 2:
                        digit = x_decoded[0].reshape(org_image_size[0], org_image_size[1])

                    figure[i * org_image_size[0]: (i + 1) * org_image_size[0],
                        j * org_image_size[0]: (j + 1) * org_image_size[0]] = digit

            plt.figure(figsize=(10, 10))
            plt.imshow(figure, cmap='Greys_r')
            plt.show()

    def model_dense_layer(self, layer_size, layer_activation_func, name=None):
        """
        Creates a dense layer with the given parameter.

        Parameter:
            layer_size: The size of the layer.
            layer_activation_func: The activation function of the layer.

        Return:
            The created dense layer.
        """
        return Dense(layer_size,
                     activation=layer_activation_func,
                     #ToDo: should be np.random.normal(0, 1, size=size) / np.sqrt(size[1])
                     #kernel_initializer='random_normal', 
                     #kernel_initializer=self.valriable_initialization,
                     #bias_initializer='random_normal',
                     name=name)

    def valriable_initialization(self, shape, name=None):
        # value = np.random.normal(0, 1, size=shape) / np.sqrt(shape[1])
        return K.variable(np.random.uniform(0.0, 1.0, shape) / np.sqrt(shape[1]), name=name)

    def bias_initializer(self, shape, name=None):
        return K.variable(np.random.normal(0, 1, shape))

    def encoder(self, input_layer):
        """
        Construct the encoder network of the model. The network input is connected to the given
        input layer.

        Parameter:
            input_layer: The input layer of the model.

        Return:
            he constructed encoder network.
        """
        encoder = self.model_dense_layer(self.encoder_layer_sizes[0],
                                         self.encoder_activation_func,
                                         name="encoder_layer_0")(input_layer)

        for i in range(1, len(self.encoder_layer_sizes)):
            encoder = self.model_dense_layer(self.encoder_layer_sizes[i],
                                             self.encoder_activation_func,
                                             name="encoder_layer_"+str(i))(encoder)

        return encoder

    def comput_latent_variable(self, encoder):
        q_mean = Dense(self.latent_dimension, name="q_mean")(encoder)
        q_log_var = Dense(self.latent_dimension)(encoder)

        def sampling(args):
            q_mean, q_log_var = args
            # reparametrization trick
            epsilon = K.random_normal(shape=(self.batch_size, self.latent_dimension),
                                              mean=0.,
                                              stddev=1.0)

            return q_mean + K.exp(q_log_var / 2) * epsilon

        # note that "output_shape" isn't necessary with the TensorFlow backend
        z = Lambda(sampling, output_shape=(self.latent_dimension,))([q_mean, q_log_var])

        return z, q_log_var, q_mean


    def decoder(self, z):
        """
        Construct the decoder network of the model. The network input is connected to the given
        encoder output.

        Parameter:
            encoder: The output of the encoder network.

        Return:
            The constructed decoder network.
        """
        self.decoder_layer = []
        layer = self.model_dense_layer(self.decoder_layer_sizes[0],
                                         self.decoder_activation_func,
                                         name="decoder_layer_0")

        self.decoder_layer.append(layer)
        decoder = layer(z)

        for i in range(1, len(self.decoder_layer_sizes)):
            layer = self.model_dense_layer(self.decoder_layer_sizes[i],
                                             self.decoder_activation_func,
                                             name="decoder_layer_"+str(i))
            self.decoder_layer.append(layer)   
            decoder = layer(decoder)

        return decoder

    # Compute likelihood lower bound given a variational auto-encoder
    # L is number of samples
    def est_loglik(self, x, n_batch, n_samples=1, byteToFloat=False):
        
        n_tot = iter(x.values()).next().shape[1]
        
        px = 0 # estimate of marginal likelihood
        lowbound = 0 # estimate of lower bound of marginal likelihood
        for _ in range(n_samples):
            _L = np.zeros((1,0))
            i = 0
            while i < n_tot:
                i_to = min(n_tot, i+n_batch)
                _x = ndict.getCols(x, i, i_to)
                if byteToFloat: _x = {i:_x[i].astype(np.float32)/256. for i in _x}
                _L = np.hstack((_L, self.eval(_x, {})))
                i += n_batch
            lowbound += _L.mean()
            px += np.exp(_L)
        
        lowbound /= n_samples
        logpx = np.log(px / n_samples).mean()
        return lowbound, logpx


    def calc_loss(self, x, output):
        """
        Calculate the loss.

        Retruns:
            The loss value.
        """
        if self.output_format_distribution is "bernulli":
            xent_loss = self.input_dim * metrics.binary_crossentropy(x, output)
        elif self.output_format_distribution is "gaussian":
            n = pdf.normal2(x, output, self.decoder_logvar_output)
            xent_loss = K.sum(n)

        kl_loss = - 0.5  * K.sum(1 + self.q_log_var - K.square(self.q_mean) - K.exp(self.q_log_var), axis=-1)
        return xent_loss + kl_loss 

   