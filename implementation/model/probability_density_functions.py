"""
This module provides functions to calulate propability densitys.
Autor: Robert Kasseck
"""
import math
import keras.backend as K

C = 0.5 * math.log(2 * math.pi)

def normal(x, mean, sd):
    #c - T.log(T.abs_(sd)) - (x - mean)**2 / (2 * sd**2)
    return C - K.log(K.abs(sd)) - (x - mean)**2 / (2 * sd**2)


def normal2(x, mean, logvar):
    return C + logvar / 2 + (x - mean)**2 / (2 * K.exp(logvar))


def laplace(x, mean, logvar):
    sd = K.exp(0.5 * logvar)
    return - abs(x - mean) / sd - 0.5 * logvar - math.log(2)

def standard_normal(x):
    return C - x**2 / 2

# Centered laplace with unit scale (b=1)
def standard_laplace(x):
    return math.log(0.5) - K.abs(x)

# Centered student-t distribution
# v>0 is degrees of freedom
# See: http://en.wikipedia.org/wiki/Student's_t-distribution
def studentt(x, v):
    gamma1 = misc.log_gamma_lanczos((v + 1) / 2.)
    gamma2 = misc.log_gamma_lanczos(0.5 * v)

    return gamma1 - 0.5 * K.log(v * math.pi) - gamma2 \
           - (v+1)/2. * K.log(1 + (x*x)/v)
