"""
Autor: Robert Kasseck
"""

from keras.layers import multiply, Input, Dense, Lambda, concatenate
from keras.layers.merge import Add
import keras.backend as K
from keras import metrics
from keras import optimizers
from keras.models import Model
from keras.callbacks import TensorBoard, ModelCheckpoint
from . import probability_density_functions as pdf
import numpy as np
from keras.regularizers import l2
from keras.activations import softplus
import tensorflow as tf
from .misc import binarize_labels
import os

class VariationalAutoEncoder2():
    """
    This class is a base class for an vaiational auto encoder
    """


    def __init__(self,
                 input_dim,
                 output_dim,
                 num_classes,
                 batch_size,
                 encoder_layer_sizes,
                 latent_dimension,
                 decoder_layer_sizes,
                 encoder_activation_func='relu',
                 decoder_activation_func='relu',
                 weight_decay = 0.,
                 var_smoothing=0,
                 learning_rate = 0.001):
                 
        self.input_dim = input_dim
        self.output_dim = output_dim
        self.num_classes = num_classes
        self.batch_size = batch_size
        self.x_input = Input(batch_shape=(self.batch_size, self.input_dim), name="x_input")
        self.y_input = Input(batch_shape=(self.batch_size, self.num_classes), name="y_input")
                
        self.encoder_layer_sizes = encoder_layer_sizes
        self.latent_dimension = latent_dimension #n_z
        self.decoder_layer_sizes = decoder_layer_sizes
        
        self.encoder_activation_func = encoder_activation_func
        self.decoder_activation_func = decoder_activation_func
        self.var_smoothing = var_smoothing
        self.n_mixture = 2 #ToDo: was macht das

        print("input_dim: ", input_dim)
        print("output_dim: ", output_dim)
        print("num_classes: ", num_classes)
        print("batch_size: ", batch_size)
        print("x_input: ", self.x_input)
        print("y_input: ", self.y_input)
        print("encoder_layer_sizes: ", encoder_layer_sizes)
        print("latent_dimension: ", latent_dimension)
        print("decoder_layer_sizes: ", decoder_layer_sizes)
        print("encoder_activation_func: ", encoder_activation_func)
        print("decoder_activation_func: ", decoder_activation_func)
        print("var_smoothing: ", var_smoothing)
        print("n_mixture: ", self.n_mixture)

        self.encoder_layer = self.encoder()
        self.z, self.q_log_var, self.q_mean = self.comput_latent_variable(self.encoder_layer)

        self.decoder_inputs, self.decoder_output_layer = self.decoder(self.z, self.y_input)
        
        self.decoder_mean = Dense(self.output_dim, activation='sigmoid', name='decoder_mean')
        decoded_mean = self.decoder_mean(self.decoder_output_layer)

        self.model = Model(inputs=[self.x_input, self.y_input], outputs=[decoded_mean])
        # orginal settings
        optimizer = optimizers.adam(learning_rate, 0.9, 0.999, decay=0) #weight_decay)
        # self.model.compile(optimizer=optimizer, loss=self.calc_loss)

        #optimizer = optimizers.rmsprop(learning_rate, decay=weight_decay)
        self.model.compile(optimizer=optimizer, loss=self.calc_loss)
        self.model.summary()
        

    def fit(self, x_input, y_output, y, val_x_input, val_y_output, val_y, epochs=9999999,
            outputdir="", snapshot_file=None):
        start_epoch = 0
        if snapshot_file is not None:
            print("loading weights: ", snapshot_file)
            self.model.load_weights(snapshot_file)
            start_epoch = int(snapshot_file.split("-")[2])
        

        print("x_input shape: ", x_input.shape)
        print("y_output shape: ", y_output.shape)
        print("y shape: ", y.shape)
        print("val_x_input shape: ", val_x_input.shape)
        print("val_y_output shape: ", val_y_output.shape)
        print("val_y shape: ", val_y.shape)
        print("epochs: ", epochs)
        print("outputdir: ", outputdir)

        tbCallBack = TensorBoard(log_dir=outputdir, histogram_freq=0, write_graph=True, write_images=True)

        filepath = os.path.join(outputdir, 'weights-improvement-{epoch:02d}-{val_loss:.2f}.h5')
        checkpoint = ModelCheckpoint(filepath, monitor='val_loss', verbose=1, save_weights_only=True, period=100)

        self.model.fit_generator(self.myGenerator(x_input, y_output, y, True),
                                 steps_per_epoch=(len(x_input)/self.batch_size) * 10,
                                 epochs=epochs,
                                 callbacks=[tbCallBack, checkpoint],
                                 validation_data=self.myGenerator(val_x_input, val_y_output, val_y, True),
                                 validation_steps=len(val_x_input)/self.batch_size,
                                 initial_epoch=start_epoch)

        self.model.save_weights(os.path.join(outputdir, 'final_weights.h5'))

    def myGenerator(self, x_data, x_data_output, y_lables, randomize=False):
        
        # print("Generator input")
        # print("x_data shape: ", x_data.shape)
        # print("x_data_output shape: ", x_data_output.shape)
        # print("y_lables shape: ", y_lables.shape)

        from random import shuffle
        position = 0
        while 1:
            x_input = x_data[position:position + self.batch_size]
            y_input = y_lables[position:position + self.batch_size]
            output = x_data_output[position:position + self.batch_size]
            yield {'x_input': x_input, 'y_input': y_input}, output
            position += self.batch_size
            if position + self.batch_size > len(x_data):
                position = 0
                if randomize:
                    x_shuf = []
                    y_shuf = []
                    x_y_shuf = []
                    index_shuf = list(range(len(x_data)))
                    shuffle(index_shuf)
                    for i in index_shuf:
                        x_shuf.append(x_data[i])
                        y_shuf.append(y_lables[i])
                        x_y_shuf.append(x_data_output[i])

                    x_data = np.asarray(x_shuf)
                    y_lables = np.asarray(y_shuf)
                    x_data_output = np.asarray(x_y_shuf)

    def test(self, train_class_y, test_x, test_class_y, n_samples=1000, saved_weights_path=None):
        if saved_weights_path is not None:
            print("loading weights: ", saved_weights_path)
            self.model.load_weights(saved_weights_path, True)

        def get_lowerbound(p_test_x):
            n_y = self.num_classes
            lb = np.zeros((p_test_x.shape[0], n_y))
            for _class in range(n_y):
                y = np.zeros((p_test_x.shape[0], n_y))
                y[:,_class] = 1
                for i in range(0, len(p_test_x), self.batch_size):
                    data = p_test_x[i:i+self.batch_size].reshape(self.batch_size, p_test_x[i].shape[0])
                    lable = y[i:i+self.batch_size].reshape(self.batch_size, y[i].shape[0])
                    ev = self.model.evaluate({'x_input': data, 'y_input': lable},
                                             data,
                                             self.batch_size,
                                             0)

                    lb[i:i+self.batch_size,_class] = ev # * -1
            #print(lb)
            return lb#.transpose()

        def get_posterior(likelihood, prior):
            posterior = (likelihood * prior)
            posterior /= posterior.sum(axis=1, keepdims=True)
            return posterior

        def get_predictions(prior_y,
                            test_x,
                            test_y,
                            n_samples=1000,
                            show_convergence=True):
            px = 0
            for i in range(n_samples):
                px += np.exp(get_lowerbound(test_x))
                #px += get_lowerbound(test_x)
                
                if show_convergence:
                    posterior = get_posterior(px / float(i+1), prior_y)
                    #pred = np.argmax(posterior, axis=1)
                    #print(posterior)
                    print(list(test_y))
                    pred = np.argmin(posterior, axis=1)
                    #print(test_y)
                    print(pred)
                    error_perc = 100* (pred != test_y).sum() / (1.*test_y.shape[0])
                    print('samples:', i, ', test-set error (%):', error_perc)

            posterior = get_posterior(px / n_samples, prior_y)
            return np.argmax(posterior, axis=1)

        prior_y = train_class_y.mean(axis=0).reshape((1,10))
        #prior_y = 1 - prior_y
        #prior_y = prior_y.transpose()

        print('Computing class posteriors using a marginal likelihood estimate with importance sampling using ', n_samples, ' samples.')
        print('This is slow, but could be sped up significantly by fitting a classifier to match the posteriors (of the generative model) in the training set.')
        print('For MNIST, this should converge to ~ 0.96 % error.')
        result = get_predictions(prior_y, test_x[0:20], test_class_y[0:20], n_samples, show_convergence=True)
        #result = get_predictions(prior_y, test_x, test_class_y, n_samples, show_convergence=True)
        print('Done.')
        print('Result (test-set error %): ', result)
        


    def generate(self, x, y, org_image_size, generate_from_x=False, snapshot_file=None):
        if snapshot_file is not None:
            print("loading weights: ", snapshot_file)
            self.model.load_weights(snapshot_file)
            start_epoch = int(snapshot_file.split("-")[2])

        # # build a model to project inputs on the latent space
        encoder = Model(inputs=[self.x_input, self.y_input], outputs=[self.q_mean])

        decoder_input = Input(shape=(self.latent_dimension,), name="decoder_input")
        lable_input = Input(shape=(self.num_classes,), name="lable_input")
        x_input_layer = self.decoder_inputs[0](decoder_input)
        y_input_layer = self.decoder_inputs[1](lable_input)
        # decoder = concatenate([x_input_layer, y_input_layer])

        a = self.decoder_add([x_input_layer, y_input_layer])
        decoder = self.decoder_layer[0](a)


        # build a digit generator that can sample from the learned distribution
        for i in range(1, len(self.decoder_layer)):
            layer = self.decoder_layer[i]
            decoder = layer(decoder)


        _x_decoded_mean = self.decoder_mean(decoder)
        generator = Model(inputs=[decoder_input, lable_input], outputs=[_x_decoded_mean])

        import matplotlib.pyplot as plt
        # display a 2D plot of the digit classes in the latent space
        # x_encoded = encoder.predict(x, batch_size=self.batch_size)
        # plt.figure(figsize=(6, 6))
        # plt.scatter(x_encoded[:, 0], x_encoded[:, 1], c=y)
        # plt.colorbar()
        # plt.show()

      
        if generate_from_x:
            for i in range(len(x)):
                z = encoder.predict({'x_input':x[i].reshape(1, x[i].shape[0]), 'y_input':y[i].reshape(1, y[i].shape[0])})
                image = generator.predict({'decoder_input':z, 'lable_input':y[i].reshape(1, y[i].shape[0])})

                if len(org_image_size) == 3:
                    digit = image[0].reshape(org_image_size[0], org_image_size[1], org_image_size[2])
                    org = x[i].reshape(1, x[i].shape[0]).reshape(org_image_size[0], org_image_size[1], org_image_size[2])
                elif len(org_image_size) == 2:
                    digit = image[0].reshape(org_image_size[0], org_image_size[1])
                    org = x[i].reshape(1, x[i].shape[0]).reshape(org_image_size[0], org_image_size[1])

                from PIL import Image
                if len(org_image_size) == 3:
                    im = Image.fromarray(np.uint8(digit*255))
                elif len(org_image_size) == 2:
                    im = Image.fromarray(digit * 255).convert('RGB')
                im.save("implementation/m2out/" + str(i) + ".png")

                orgout = Image.fromarray(np.uint8(org * 255))
                orgout.save("implementation/m2out/" + str(i) + "_org.png")


        else:
            # display a 2D manifold of the digits
            n = 15  # figure with 15x15 digits
            if len(org_image_size) == 3:
                figure = np.zeros((org_image_size[0] * n, org_image_size[1] * n, org_image_size[2]))
            elif len(org_image_size) == 2:
                figure = np.zeros((org_image_size[0] * n, org_image_size[1] * n))
            # linearly spaced coordinates on the unit square were transformed through
            # the inverse CDF (ppf) of the Gaussian to produce values of the latent
            # variables z, since the prior of the latent space is Gaussian
            from scipy.stats import norm
            grid_x = norm.ppf(np.linspace(0.05, 0.95, n))
            grid_y = norm.ppf(np.linspace(0.05, 0.95, n))

            for lable in range(self.num_classes):
                lable_name = lable
                lable = binarize_labels(np.asarray([lable]), self.num_classes)
                for i, yi in enumerate(grid_x):
                    for j, xi in enumerate(grid_y):
                        z_sample = np.array([[xi, yi]])
                        x_decoded = generator.predict({'decoder_input': z_sample, 'lable_input': lable})
                        if len(org_image_size) == 3:
                            digit = x_decoded[0].reshape(org_image_size[0], org_image_size[1], org_image_size[2])
                        elif len(org_image_size) == 2:
                            digit = x_decoded[0].reshape(org_image_size[0], org_image_size[1])
                        from PIL import Image
                        data = digit*255
                        if len(org_image_size) == 3:
                            im = Image.fromarray(np.uint8(digit*255))
                        elif len(org_image_size) == 2:
                            im = Image.fromarray(digit * 255).convert('RGB')
                        im.save('implementation/m2out/' + str(lable_name) + '/' + str(yi) + "_" + str(xi) + ".png")

                        figure[i * org_image_size[0]: (i + 1) * org_image_size[0],
                        j * org_image_size[0]: (j + 1) * org_image_size[0]] = digit

                plt.figure(figsize=(10, 10))
                plt.imshow(figure, cmap='Greys_r')
                plt.show()

    def model_dense_layer(self, layer_size, layer_activation_func, name=None):
        """
        Creates a dense layer with the given parameter.

        Parameter:
            layer_size: The size of the layer.
            layer_activation_func: The activation function of the layer.

        Return:
            The created dense layer.
        """
        return Dense(layer_size,
                     activation=layer_activation_func,
                     #ToDo: should be np.random.normal(0, 1, size=size) / np.sqrt(size[1])
                     #kernel_initializer=self.valriable_initialization,
                     bias_initializer='random_normal',
                     name=name)

    def valriable_initialization(self, shape, name=None):
        # value = np.random.normal(0, 1, size=shape) / np.sqrt(shape[1])
        return K.variable(np.random.uniform(0.0, 1.0, shape) / np.sqrt(shape[1]), name=name)

    def bias_initializer(self, shape, name=None):
        return K.variable(np.random.normal(0, 1, shape))

    def encoder(self):
        """
        Construct the encoder network of the model. The network input is connected to the given
        input layer.

        Return:
            he constructed encoder network.
        """
        # y = Dense(self.num_classes, 
        #           #kernel_initializer=self.valriable_initialization,
        #           activation=self.encoder_activation_func)(self.y_input)
        # x = Dense(self.encoder_layer_sizes[0], 
        #           #kernel_initializer=self.valriable_initialization,
        #           #bias_initializer=self.bias_initializer,
        #           bias_initializer='random_normal',
        #           activation=self.encoder_activation_func,
        #           name = "encoder_layer_0")(self.x_input)

        #hidden_q = [nonlinear_q(T.dot(v['w0x'], x['x']) + T.dot(v['w0y'], x['y']) + T.dot(v['b0'], A))]

        #encoder = concatenate([y, x]) 


        y = Dense(self.encoder_layer_sizes[0], 
                  use_bias=False,
                  activation='linear',
                  name="encoder_y_input")(self.y_input)
        x = Dense(self.encoder_layer_sizes[0], 
                  use_bias=True,
                  activation='linear',
                  name = "encoder_x_input")(self.x_input)

        a = Add()([x, y])
        encoder = Dense(self.encoder_layer_sizes[0],
                        bias_initializer='random_normal',
                        activation=self.encoder_activation_func)(a)

        


        for i in range(1, len(self.encoder_layer_sizes)):
            encoder = self.model_dense_layer(self.encoder_layer_sizes[i],
                                             self.encoder_activation_func,
                                             name="encoder_layer_"+str(i))(encoder)

        return encoder

    def comput_latent_variable(self, encoder):
        q_mean = Dense(self.latent_dimension)(encoder)
        q_log_var = Dense(self.latent_dimension)(encoder)

        def sampling(args):
            q_mean, q_log_var = args
            # reparametrization trick
            epsilon = K.random_normal(shape=(self.batch_size, self.latent_dimension),
                                                   mean=0.,
                                                   stddev=1.0,
                                                   dtype='float32',
                                                   seed=1)

            return q_mean + K.exp(q_log_var / 2) * epsilon

        # note that "output_shape" isn't necessary with the TensorFlow backend
        z = Lambda(sampling, output_shape=(self.latent_dimension,))([q_mean, q_log_var])
        return z, q_log_var, q_mean

    def decoder(self, z, y):
        """
        Construct the decoder network of the model. The network input is connected to the given
        encoder output.

        Parameter:
            encoder: The output of the encoder network.

        Return:
            The constructed decoder network.
        """
        self.decoder_layer = []
        # label_input = Dense(self.num_classes, 
        #           #kernel_initializer=self.valriable_initialization,
        #           activation=self.encoder_activation_func)
        # y = label_input(y)
        # decoder_input = Dense(self.decoder_layer_sizes[0], 
        #           #kernel_initializer=self.valriable_initialization,
        #           #bias_initializer=self.bias_initializer,
        #           bias_initializer='random_normal',
        #           activation=self.encoder_activation_func)
        # z = decoder_input(z)
        # decoder = concatenate([z, y])

        label_input = Dense(self.decoder_layer_sizes[0], 
                  use_bias=True,
                  activation='linear',
                  name="decoder_y_input")
        y = label_input(self.y_input)
                  
        decoder_input = Dense(self.decoder_layer_sizes[0], 
                  use_bias=False,
                  activation='linear',
                  name = "decoder_x_input")
        zInput = decoder_input(z)

        self.decoder_add = Add()
        a = self.decoder_add([zInput, y])
        layer = Dense(self.decoder_layer_sizes[0],
                        bias_initializer='random_normal',
                        activation=self.encoder_activation_func)
        self.decoder_layer.append(layer)    
        decoder = layer(a)


        #hidden_p = [nonlinear_p(T.dot(w['w0y'], x['y']) + T.dot(w['w0z'], _z) + T.dot(w['b0'], A))]

        for i in range(1, len(self.decoder_layer_sizes)):
            layer = self.model_dense_layer(self.decoder_layer_sizes[i],
                                             self.decoder_activation_func,
                                             name="decoder_layer_"+str(i))                                
            self.decoder_layer.append(layer)   
            decoder = layer(decoder)

        return [decoder_input, label_input], decoder


    def calc_loss(self, x, decoder_mean):
        """
        Calculate the loss.

        Retruns:
            The loss value.
        """
        
        xent_loss = self.input_dim * metrics.binary_crossentropy(x, decoder_mean)

        #gaussianmarg
        logpz = K.mean(- 0.5 * (np.log(2 * np.pi) - (self.q_mean**2 + K.exp(self.q_log_var))))
        #gaussian
        #logpz = K.mean(logpdfs.standard_normal(self.z))

        #gaussianmarg
        logqz = K.mean(- 0.5 * (np.log(2 * np.pi) - self.q_log_var))
        #gaussian
        #logqz = K.mean(logpdfs.normal2(self.z, self.q_mean, self.q_log_var))

        xent_loss = K.mean(xent_loss) 

        #xent_loss = K.print_tensor(xent_loss)
        #logpz = K.print_tensor(logpz)
        #logqz = K.print_tensor(logqz)

        #kl_loss = - 0.5 * K.sum(1 + z_log_var - K.square(z_mean) - K.exp(z_log_var), axis=-1)
        kl_loss = - 0.5  * K.sum(1 + self.q_log_var - K.square(self.q_mean) - K.exp(self.q_log_var), axis=-1)
        #return K.mean(xent_loss + kl_loss)
        return xent_loss + (logpz - logqz)
        #return logpz