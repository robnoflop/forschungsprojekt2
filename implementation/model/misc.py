import math
import numpy as np
import keras.backend as keras_backend

def random_orthogonal(dim, special=True):
    if dim == 1:
        if np.random.uniform() < 0.5:
            return np.ones((1, 1))
        return -np.ones((1, 1))
    P = np.random.randn(dim, dim)
    while np.linalg.matrix_rank(P) != dim:
        P = np.random.randn(dim, dim)
    U, _, V = np.linalg.svd(P)
    P = np.dot(U, V)
    if special:
        # Make sure det(P) == 1
        if np.linalg.det(P) < 0:
            P[:, [0, 1]] = P[:, [1, 0]]
    return P

# Code From:
# https://gist.github.com/benanne/2025317
# (Checked for correctness against scipy)

# Log-gamma function for theano
def log_gamma_lanczos(z):
    # reflection formula. Normally only used for negative arguments,
    # but here it's also used for 0 < z < 0.5 to improve accuracy in this region.
    flip_z = 1 - z
    # because both paths are always executed (reflected and non-reflected),
    # the reflection formula causes trouble when the input argument is larger than one.
    # Note that for any z > 1, flip_z < 0.
    # To prevent these problems, we simply set all flip_z < 0 to a 'dummy' value.
    # This is not a problem, since these computations are useless anyway and
    # are discarded by the T.switch at the end of the function.
    flip_z = keras_backend.switch(flip_z < 0, 1, flip_z)
    small = math.log(np.pi) - keras_backend.log(keras_backend.sin(np.pi * z)) - \
            log_gamma_lanczos_sub(flip_z)
    big = log_gamma_lanczos_sub(z)
    return keras_backend.switch(z < 0.5, small, big)

## version that isn't vectorised, since g is small anyway
def log_gamma_lanczos_sub(z): #expanded version
    # Coefficients used by the GNU Scientific Library
    g = 7
    p = np.array([0.99999999999980993, 676.5203681218851, -1259.1392167224028,
                  771.32342877765313, -176.61502916214059, 12.507343278686905,
                  -0.13857109526572012, 9.9843695780195716e-6, 1.5056327351493116e-7])
    z = z - 1
    x = p[0]
    for i in range(1, g+2):
        x += p[i]/(z+i)
    t = z + g + 0.5
    return math.log(np.sqrt(2 * np.pi)) + (z + 0.5) * keras_backend.log(t) - \
                    t + keras_backend.log(x)

def binarize_labels(y, n_classes=10):
    new_y = np.zeros((y.shape[0], n_classes))
    for i in range(y.shape[0]):
        new_y[i, y[i]] = 1
    return new_y