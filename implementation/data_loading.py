import os
import numpy as np
from PIL import Image
import pickle, gzip

def load_mnist(flatten=True):
    with open('data/mnist.pkl', 'rb') as f:
        u = pickle._Unpickler(f)
        u.encoding = 'latin1'
        train, valid, test = u.load()
        f.close()
        train_x, train_y = train
        valid_x, valid_y = valid
        test_x, test_y = test

    train_x = train_x.astype('float32')
    valid_x = valid_x.astype('float32')
    test_x = test_x.astype('float32')

    if not flatten:
        train_x = train_x.reshape((len(train_x), 28, 28, 1))
        valid_x = valid_x.reshape((len(valid_x), 28, 28, 1))
        test_x = test_x.reshape((len(test_x), 28, 28, 1))

    return train_x, train_y, valid_x, valid_y, test_x, test_y


def load_nucli_gray(flatten=True):
    data_x = []
    print("loading data")
    dataPath = "data/DetectionSmall/dataGray"
    for root, dirs, files in os.walk(dataPath):
        files.sort()
        for name in files:
            x = Image.open(os.path.join(root, name))
            data_x.append(np.asarray(x))
            x.close()

    test_x = []
    valid_x = []
    train_x = []
    for i in range(len(data_x)):
        if i % 10 == 0:
            test_x.append(data_x[i])
        elif i % 10 == 1 or i % 10 == 2 or i % 10 == 3:
            valid_x.append(data_x[i])
        else:
            train_x.append(data_x[i])

    test_x = np.asarray(test_x)
    valid_x = np.asarray(valid_x)
    train_x = np.asarray(train_x)

    if flatten:
        test_x = test_x.reshape((len(test_x), np.prod(test_x.shape[1:])))
        valid_x = valid_x.reshape((len(valid_x), np.prod(valid_x.shape[1:])))
        train_x = train_x.reshape((len(train_x), np.prod(train_x.shape[1:])))
    else:
        test_x = test_x.reshape((len(test_x), 64, 64, 1))
        valid_x = valid_x.reshape((len(valid_x), 64, 64, 1))
        train_x = train_x.reshape((len(train_x), 64, 64, 1))

    train_x = train_x.astype('float32') / 255.
    valid_x = valid_x.astype('float32') / 255.
    test_x = test_x.astype('float32') / 255.

    return train_x, train_x, valid_x, valid_x, test_x, test_x

def load_nucli_rgb_gray(flatten=True):
    data_x = []
    data_y = []
    print("loading data")
    dataPath = "data/DetectionSmall/data"
    for root, dirs, files in os.walk(dataPath):
        files.sort()
        for name in files:
            x = Image.open(os.path.join(root, name))
            data_x.append(np.asarray(x))
            #data_x.append(np.reshape(np.array(list(x.getdata())), [64,64,3]))
            x.close()

    print("loading lables")
    dataPath = "data/DetectionSmall/dataGray"
    for root, dirs, files in os.walk(dataPath):
        files.sort()
        for name in files:
            y = Image.open(os.path.join(root, name))
            data_y.append(np.asarray(y))
            #data_y.append(np.reshape(np.array(list(y.getdata())), [64,64,1]))
            y.close()

    test_x, test_y = [], []
    valid_x, valid_y = [], []
    train_x, train_y = [], []
    for i in range(len(data_x)):
        if i % 10 == 0:
            test_x.append(data_x[i])
            test_y.append(data_y[i])
        elif i % 10 == 1 or i % 10 == 2 or i % 10 == 3:
            valid_x.append(data_x[i])
            valid_y.append(data_y[i])
        else:
            train_x.append(data_x[i])
            train_y.append(data_y[i])

    test_x, test_y = np.asarray(test_x), np.asarray(test_y)
    valid_x, valid_y = np.asarray(valid_x), np.asarray(valid_y)
    train_x, train_y = np.asarray(train_x), np.asarray(train_y)

    if flatten:
        test_x = test_x.reshape((len(test_x), np.prod(test_x.shape[1:])))
        test_y = test_y.reshape((len(test_y), np.prod(test_y.shape[1:])))
        valid_x = valid_x.reshape((len(valid_x), np.prod(valid_x.shape[1:])))
        valid_y = valid_y.reshape((len(valid_y), np.prod(valid_y.shape[1:])))
        train_x = train_x.reshape((len(train_x), np.prod(train_x.shape[1:])))
        train_y = train_y.reshape((len(train_y), np.prod(train_y.shape[1:])))

    train_x = train_x.astype('float32') / 255.
    valid_x = valid_x.astype('float32') / 255.
    test_x = test_x.astype('float32') / 255.
    train_y = train_y.astype('float32') / 255.
    valid_y = valid_y.astype('float32') / 255.
    test_y = test_y.astype('float32') / 255.

    return train_x, train_y, valid_x, valid_y, test_x, test_y

def load_nucli_rgb(flatten=True):
    data_x = []
    print("loading data")
    dataPath = "data/DetectionSmall/data"
    for root, dirs, files in os.walk(dataPath):
        files.sort()
        for name in files:
            x = Image.open(os.path.join(root, name))
            data_x.append(np.asarray(x))
            x.close()

    test_x = []
    valid_x = []
    train_x = []
    for i in range(len(data_x)):
        if i % 10 == 0:
            test_x.append(data_x[i])
        elif i % 10 == 1 or i % 10 == 2 or i % 10 == 3:
            valid_x.append(data_x[i])
        else:
            train_x.append(data_x[i])

    test_x = np.asarray(test_x)
    valid_x = np.asarray(valid_x)
    train_x = np.asarray(train_x)

    if flatten:
        test_x = test_x.reshape((len(test_x), np.prod(test_x.shape[1:])))
        valid_x = valid_x.reshape((len(valid_x), np.prod(valid_x.shape[1:])))
        train_x = train_x.reshape((len(train_x), np.prod(train_x.shape[1:])))

    train_x = train_x.astype('float32') / 255.
    valid_x = valid_x.astype('float32') / 255.
    test_x = test_x.astype('float32') / 255.

    return train_x, train_x, valid_x, valid_x, test_x, test_x
