"""
Autor: Robert Kasseck

tensorboard --logdir=1:1,2:2,3:3,4:4,5:5,6:6,7:7,8:8,9:9,10:10,11:11,12:12,13:13,14:14,15:15,16:16,17:17,18:18,19:19,20:20,21:21,22:22,23:23,24:24

"""
import numpy as np
import os
from PIL import Image

import pickle, gzip

import data_loading as data


dataset = "mnist"
dataset = "nucliGray"
#dataset = "nucliRgbGray"
#dataset = "nucliRgb"

#with gzip.open('implementation/mnist_28.pkl.gz', 'rb') as pkl:
#with open('implementation/mnist.pkl', 'rb') as f:
if dataset is "mnist":
    
    train_x, train_y, valid_x, valid_y, test_x, test_y = data.load_mnist()
    
    #layers = [500, 500]
    layers = [600, 600]
    input_dim = train_x[0].shape[0]
    output_dim = train_x[0].shape[0]
    batch_size = 10000
    org_image_size = [28, 28]
    output_format_distribution = "bernulli"

elif dataset is "nucliGray":
    
    train_x, train_y, valid_x, valid_y, test_x, test_y = data.load_nucli_gray()

    layers = [2400, 1200]
    #layers = [500, 500]
    input_dim = train_x[0].shape[0]
    output_dim = train_x[0].shape[0]
    batch_size = 1000
    org_image_size = [64, 64]
    output_format_distribution = "gaussian"

elif dataset is "nucliRgbGray":
    
    train_x, train_y, valid_x, valid_y, test_x, test_y = data.load_nucli_rgb_gray()

    layers = [600, 600]
    input_dim = train_x[0].shape[0]
    output_dim = train_y[0].shape[0]
    batch_size = 3000
    org_image_size = [64, 64]

elif dataset is "nucliRgb":
    
    train_x, train_y, valid_x, valid_y, test_x, test_y = data.load_nucli_rgb()

    layers = [600, 600]
    layers = [600, 600, 600]
    layers = [600, 400, 200]
    layers = [1200, 600, 300]
    input_dim = train_x[0].shape[0]
    output_dim = train_x[0].shape[0]
    batch_size = 1000
    org_image_size = [64, 64, 3]


epochs = 100000
latent_dim = 200
activation_func = 'softplus'
learning_rate = 0.0001

from model.model1 import VariationalAutoEncoder1
model = VariationalAutoEncoder1(input_dim = input_dim,
                               output_dim = output_dim,
                               batch_size = batch_size,
                               encoder_layer_sizes = layers,
                               latent_dimension = latent_dim,
                               decoder_layer_sizes = layers[::-1],
                               encoder_activation_func=activation_func,
                               decoder_activation_func=activation_func,
                               output_format_distribution=output_format_distribution,
                               decay = 0.00001, #batch_size/len(train_x),
                               learning_rate = learning_rate)

if dataset is "mnist" or dataset is "nucliGray" or dataset is "nucliRgb":
    model.fit(train_x, train_x,
            valid_x, valid_x,
            epochs,
            outputdir="implementation/experiments/testM1") #, 
            #snapshot_file="implementation/experiments/33/weights-improvement-16699-2762736.64.h5")
elif dataset is "nucliRgbGray":
    model.fit(train_x, train_y,
            valid_x, valid_y,
            epochs,
            outputdir="implementation/experiments/testM1")
