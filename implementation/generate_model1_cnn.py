"""
Autor: Robert Kasseck

tensorboard --logdir=1:1,2:2,3:3,4:4,5:5,6:6,7:7,8:8,9:9,10:10,11:11,12:12,13:13,14:14,15:15,16:16,17:17,18:18,19:19,20:20,21:21,22:22,23:23,24:24

"""
import numpy as np
import data_loading as data

dataset = "mnist"
dataset = "nucliGray"
#dataset = "nucliRgbGray"
#dataset = "nucliRgb"

if dataset is "mnist":
    
    train_x, train_y, valid_x, valid_y, test_x, test_y = data.load_mnist(flatten=False)
    
    layers = [64, 64]
    batch_size = 1000
    org_image_size = [28, 28]
    output_format_distribution = "bernulli"

elif dataset is "nucliGray":
    
    train_x, train_y, valid_x, valid_y, test_x, test_y = data.load_nucli_gray(flatten=False)

    layers = [64, 64, 64, 64]
    batch_size = 200
    org_image_size = [64, 64]
    output_format_distribution = "gaussian"

elif dataset is "nucliRgbGray":
    
    train_x, train_y, valid_x, valid_y, test_x, test_y = data.load_nucli_rgb_gray()

    layers = [600, 600]
    batch_size = 3000
    org_image_size = [64, 64]

elif dataset is "nucliRgb":
    
    train_x, train_y, valid_x, valid_y, test_x, test_y = data.load_nucli_rgb()

    layers = [600, 600]
    batch_size = 1000
    org_image_size = [64, 64, 3]


epochs = 100000
latent_dim = 100
activation_func = 'relu'
convModel = False
learning_rate = 0.0001
input_dim = train_x[0].shape
output_dim = train_x[0].shape

from model.model1cnn import VariationalAutoEncoderCnn1
model = VariationalAutoEncoderCnn1(input_dim = input_dim,
                               output_dim = output_dim,
                               batch_size = batch_size,
                               encoder_layer_sizes = layers,
                               latent_dimension = latent_dim,
                               decoder_layer_sizes = layers[::-1],
                               encoder_activation_func=activation_func,
                               decoder_activation_func=activation_func,
                               output_format_distribution=output_format_distribution,
                               decay = 0.00001, #batch_size/len(train_x),
                               learning_rate = learning_rate)

model.generate(org_image_size, test_x[0:100], generate_from_x=True,
               snapshot_file="implementation/experiments/testcnn1/weights-improvement-799--0.31.h5")