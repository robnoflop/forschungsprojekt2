"""
Autor: Robert Kasseck

# """

from model2 import VariationalAutoEncoder2
import numpy as np
import pickle, gzip
import os
import logging
from keras.datasets import mnist
from PIL import Image
from misc import binarize_labels


dataset = "mnist"
#dataset = "nucliGray"
#dataset = "nucliRgbGray"
#dataset = "nucliRgb"

if dataset is "mnist":
    
    print("load mnist dataset")
    
    #with gzip.open('implementation/mnist_28.pkl.gz', 'rb') as pkl:
    with open('data/mnist.pkl', 'rb') as f:
    #with open('mnist.pkl', 'rb') as f:
        u = pickle._Unpickler(f)
        u.encoding = 'latin1'
        train, valid, test = u.load()
        f.close()
        train_x, train_class_y = train
        valid_x, valid_class_y = valid
        test_x, test_class_y = test

        # test_class_y = train_class_y
        # test_x = train_x

    train_class_y = binarize_labels(train_class_y)
    valid_class_y = binarize_labels(valid_class_y)
    test_class_y_binarized = binarize_labels(test_class_y)

    train_x = train_x[:,:].astype(np.float32)
    train_class_y = train_class_y[:,:].astype(np.float32)
    valid_x = valid_x.astype(np.float32)
    valid_class_y = valid_class_y.astype(np.float32)
    test_x = test_x.astype(np.float32)    
    test_class_y = test_class_y.astype(np.float32)
    test_class_y_binarized = test_class_y_binarized.astype(np.float32)

    train_x = train_x.astype('float32')# / 255.
    valid_x = valid_x.astype('float32')# / 255.
    test_x = valid_x.astype('float32')# / 255.
    train_x = train_x.reshape((len(train_x), np.prod(train_x.shape[1:])))
    valid_x = valid_x.reshape((len(valid_x), np.prod(valid_x.shape[1:])))
    test_x = test_x.reshape((len(test_x), np.prod(test_x.shape[1:])))


    #origial
    layers = [500, 500]
    #layers = [256, 256]
    input_dim = train_x[0].shape[0]
    output_dim = train_x[0].shape[0]
    org_image_size = [28, 28]
    num_classes = 10

elif dataset is "nucliGray":
    data_x = []
    class_y = []
    def loadClass(path, class_number):
        print("loading data")
        dataPath =   os.path.join(path, "dataGray")
        print(dataPath)
        for root, dirs, files in os.walk(dataPath):
            files.sort()
            for name in files:
                x = Image.open(os.path.join(root, name))
                data_x.append(np.asarray(x))
                x.close()
                class_y.append(class_number) 

    loadClass("data/ClassificationExtraction/epithelial", 0)
    loadClass("data/ClassificationExtraction/fibroblast", 1)
    loadClass("data/ClassificationExtraction/inflammatory", 2)
    loadClass("data/ClassificationExtraction/others", 3)

    test_x, test_class_y = [], []
    valid_x, valid_class_y = [], []
    train_x, train_class_y = [], []
    for i in range(len(data_x)):
        if i % 10 == 0:
            test_x.append(data_x[i])
            test_class_y.append(class_y[i])
        elif i % 10 == 1 or i % 10 == 2 or i % 10 == 3:
            valid_x.append(data_x[i])
            valid_class_y.append(class_y[i])
        else:
            train_x.append(data_x[i])
            train_class_y.append(class_y[i])

    test_x, test_class_y= np.asarray(test_x), np.asarray(test_class_y)
    valid_x, valid_class_y = np.asarray(valid_x), np.asarray(valid_class_y)
    train_x, train_class_y = np.asarray(train_x), np.asarray(train_class_y)

    test_class_y_binarized = binarize_labels(test_class_y, 4)
    valid_class_y = binarize_labels(valid_class_y, 4)
    train_class_y = binarize_labels(train_class_y, 4)

    test_x = test_x.reshape((len(test_x), np.prod(test_x.shape[1:])))
    valid_x = valid_x.reshape((len(valid_x), np.prod(valid_x.shape[1:])))
    train_x = train_x.reshape((len(train_x), np.prod(train_x.shape[1:])))

    train_x = train_x.astype('float32') / 255.
    valid_x = valid_x.astype('float32') / 255.
    test_x = test_x.astype('float32') / 255.

    layers = [500]
    input_dim = train_x[0].shape[0]
    output_dim = train_x[0].shape[0]
    org_image_size = [64, 64]
    num_classes = 4
elif dataset is "nucliRgbGray":
    data_x = []
    data_y = []
    class_y = []
    def loadClass(path, class_number):
        print("loading data")
        dataPath =   os.path.join(path, "data")
        print(dataPath)
        for root, dirs, files in os.walk(dataPath):
            files.sort()
            for name in files:
                x = Image.open(os.path.join(root, name))
                data_x.append(np.asarray(x))
                #data_x.append(np.reshape(np.array(list(x.getdata())), [64,64,3]))
                x.close()
                class_y.append(class_number) 

        print("loading lables")
        dataPath =   os.path.join(path, "dataGray")
        for root, dirs, files in os.walk(dataPath):
            files.sort()
            for name in files:
                y = Image.open(os.path.join(root, name))
                data_y.append(np.asarray(y))
                #data_y.append(np.reshape(np.array(list(y.getdata())), [64,64,1]))
                y.close()

    loadClass("data/ClassificationExtraction/epithelial", 0)
    loadClass("data/ClassificationExtraction/fibroblast", 1)
    loadClass("data/ClassificationExtraction/inflammatory", 2)
    loadClass("data/ClassificationExtraction/others", 3)

    

    test_x, test_y, test_class_y = [], [], []
    valid_x, valid_y, valid_class_y = [], [], []
    train_x, train_y, train_class_y = [], [], []
    for i in range(len(data_x)):
        if i % 10 == 0:
            test_x.append(data_x[i])
            test_y.append(data_y[i])
            test_class_y.append(class_y[i])
        elif i % 10 == 1 or i % 10 == 2 or i % 10 == 3:
            valid_x.append(data_x[i])
            valid_y.append(data_y[i])
            valid_class_y.append(class_y[i])
        else:
            train_x.append(data_x[i])
            train_y.append(data_y[i])
            train_class_y.append(class_y[i])

    test_x, test_y, test_class_y= np.asarray(test_x), np.asarray(test_y), np.asarray(test_class_y)
    valid_x, valid_y, valid_class_y = np.asarray(valid_x), np.asarray(valid_y), np.asarray(valid_class_y)
    train_x, train_y, train_class_y = np.asarray(train_x), np.asarray(train_y), np.asarray(train_class_y)

    test_class_y = binarize_labels(test_class_y , 4)
    valid_class_y = binarize_labels(valid_class_y, 4)
    train_class_y = binarize_labels(train_class_y, 4)

    test_x = test_x.reshape((len(test_x), np.prod(test_x.shape[1:])))
    test_y = test_y.reshape((len(test_y), np.prod(test_y.shape[1:])))
    valid_x = valid_x.reshape((len(valid_x), np.prod(valid_x.shape[1:])))
    valid_y = valid_y.reshape((len(valid_y), np.prod(valid_y.shape[1:])))
    train_x = train_x.reshape((len(train_x), np.prod(train_x.shape[1:])))
    train_y = train_y.reshape((len(train_y), np.prod(train_y.shape[1:])))

    train_x = train_x.astype('float32') / 255.
    valid_x = valid_x.astype('float32') / 255.
    test_x = test_x.astype('float32') / 255.
    train_y = train_y.astype('float32') / 255.
    valid_y = valid_y.astype('float32') / 255.
    test_y = test_y.astype('float32') / 255.

    layers = [500]
    input_dim = train_x[0].shape[0]
    output_dim = train_y[0].shape[0]
    org_image_size = [64, 64, 3]
    num_classes = 4
elif dataset is "nucliRgb":
    data_x = []
    class_y = []
    def loadClass(path, class_number):
        print("loading data")
        dataPath =   os.path.join(path, "data")
        print(dataPath)
        for root, dirs, files in os.walk(dataPath):
            files.sort()
            for name in files:
                x = Image.open(os.path.join(root, name))
                data_x.append(np.asarray(x))
                x.close()
                class_y.append(class_number) 

    loadClass("data/ClassificationExtraction/epithelial", 0)
    loadClass("data/ClassificationExtraction/fibroblast", 1)
    loadClass("data/ClassificationExtraction/inflammatory", 2)
    loadClass("data/ClassificationExtraction/others", 3)

    test_x, test_class_y = [], []
    valid_x, valid_class_y = [], []
    train_x, train_class_y = [], []
    for i in range(len(data_x)):
        if i % 10 == 0:
            test_x.append(data_x[i])
            test_class_y.append(class_y[i])
        elif i % 10 == 1 or i % 10 == 2 or i % 10 == 3:
            valid_x.append(data_x[i])
            valid_class_y.append(class_y[i])
        else:
            train_x.append(data_x[i])
            train_class_y.append(class_y[i])

    test_x, test_class_y= np.asarray(test_x), np.asarray(test_class_y)
    valid_x, valid_class_y = np.asarray(valid_x), np.asarray(valid_class_y)
    train_x, train_class_y = np.asarray(train_x), np.asarray(train_class_y)

    test_class_y = binarize_labels(test_class_y , 4)
    valid_class_y = binarize_labels(valid_class_y, 4)
    train_class_y = binarize_labels(train_class_y, 4)

    test_x = test_x.reshape((len(test_x), np.prod(test_x.shape[1:])))
    valid_x = valid_x.reshape((len(valid_x), np.prod(valid_x.shape[1:])))
    train_x = train_x.reshape((len(train_x), np.prod(train_x.shape[1:])))

    train_x = train_x.astype('float32') / 255.
    valid_x = valid_x.astype('float32') / 255.
    test_x = test_x.astype('float32') / 255.

    layers = [500] # unterfit ??
    #layers = [500, 500]
    input_dim = train_x[0].shape[0]
    output_dim = train_x[0].shape[0]
    org_image_size = [64, 64, 3]
    num_classes = 4

    
epochs = 100000
batch_size = 1
latent_dimension = 50
activation_func = 'softplus'

model = VariationalAutoEncoder2(input_dim = input_dim,
                                output_dim = output_dim,
                                num_classes = num_classes,
                                batch_size = batch_size,
                                encoder_layer_sizes = layers,
                                latent_dimension = latent_dimension,
                                decoder_layer_sizes = layers[::-1],
                                encoder_activation_func=activation_func,
                                decoder_activation_func=activation_func,
                                weight_decay = 0.0,
                                learning_rate = 0.0003) #batch_size/(len(train_x) * 4))

model.generate(test_x[0:100], test_class_y_binarized[0:100], org_image_size, 
                generate_from_x=True,
                snapshot_file="implementation/experiments/test/weights-improvement-99-57.59.h5")
