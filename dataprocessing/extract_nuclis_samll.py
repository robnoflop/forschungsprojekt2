import scipy.io
import os
from PIL import Image
from PIL import ImageDraw
from PIL import ImageChops
from PIL import ImageOps

ROOT_PATH = "C:\\Users\\RobNoFlop\\repos\\forschungsprojekt2\\data\\Detection"
OUTPUT_PATH = "C:\\Users\\RobNoFlop\\repos\\forschungsprojekt2\\data\\DetectionSmall"
RADIUS = 12
nucli_number = 0

for sub_dirs, dirs, files in os.walk(ROOT_PATH):
    if sub_dirs is not ROOT_PATH:
        img_path = os.path.join(sub_dirs, files[0])

        img = Image.open(img_path)

        mat_path = os.path.join(sub_dirs, files[1])
        mat = scipy.io.loadmat(mat_path)

        length = 64
        height = 64
        for point in mat['detection']:
            x = point[0]
            y = point[1]
            start_x = x - length / 2
            start_y = y - height / 2
            end_x = x + length / 2
            end_y = y + height / 2
            if start_x < 0:
                end_x -= start_x
                start_x -= start_x
            
            if start_y < 0:
                end_y -= start_y
                start_y -= start_y

            if end_x > img.size[0]:
                diff = end_x - img.size[0]
                end_x -= diff
                start_x -= diff

            if end_y > img.size[1]:
                diff = end_y - img.size[1]
                end_y -= diff
                start_y -= diff

            box = (start_x, start_y, end_x, end_y)
            roi = img.crop(box)

            roi.save(os.path.join(OUTPUT_PATH, "data", str(nucli_number) + ".png"))

            roi = roi.convert('L')
            roi.save(os.path.join(OUTPUT_PATH, "datagray", str(nucli_number) + ".png"))

            threshold = 185
            roi = roi.point(lambda x: 0 if x < threshold and x > 1 else 255)
            roi = ImageOps.invert(roi)
            roi = roi.convert('RGB')
            roi.save(os.path.join(OUTPUT_PATH, "lable", "label_"+str(nucli_number) + ".png"))
            nucli_number += 1

        print(files[0], " done")
        print(nucli_number, "nucli extracted")
        