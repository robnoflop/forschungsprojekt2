import scipy.io
import os
from PIL import Image
from PIL import ImageDraw
from PIL import ImageChops
from PIL import ImageOps


ROOT_PATH = "C:\\Users\\RobNoFlop\\repos\\forschungsprojekt2\\data\\Classification"
OUTPUT_PATH = "C:\\Users\\RobNoFlop\\repos\\forschungsprojekt2\\data\\ClassificationExtraction"
RADIUS = 12
epithelial_number = 0
fibroblast_number = 0
inflammatory_number = 0
other_number = 0
length = 64
height = 64


def processMat(img, mat, out_folder, nucli_number):
    out_path = os.path.join(OUTPUT_PATH, out_folder)
    for point in mat['detection']:
        x = point[0]
        y = point[1]
        start_x = x - length / 2
        start_y = y - height / 2
        end_x = x + length / 2
        end_y = y + height / 2
        if start_x < 0:
            end_x -= start_x
            start_x -= start_x
        
        if start_y < 0:
            end_y -= start_y
            start_y -= start_y

        if end_x > img.size[0]:
            diff = end_x - img.size[0]
            end_x -= diff
            start_x -= diff

        if end_y > img.size[1]:
            diff = end_y - img.size[1]
            end_y -= diff
            start_y -= diff

        box = (start_x, start_y, end_x, end_y)
        roi = img.crop(box)

        roi.save(os.path.join(out_path, "data", str(nucli_number) + ".png"))

        roi = roi.convert('L')
        roi.save(os.path.join(out_path, "datagray", str(nucli_number) + ".png"))
        threshold = 185
        roi = roi.point(lambda x: 0 if x < threshold and x > 1 else 255)
        roi = ImageOps.invert(roi)
        roi = roi.convert('RGB')
        roi.save(os.path.join(out_path, "lable", "lable_" + str(nucli_number) + ".png"))
        nucli_number += 1

    return nucli_number


for sub_dirs, dirs, files in os.walk(ROOT_PATH):
    if sub_dirs is not ROOT_PATH:
        img_path = os.path.join(sub_dirs, files[0])
        epithelial_mat_path = _mat_path = os.path.join(sub_dirs, files[1])
        fibroblast_mat_path = os.path.join(sub_dirs, files[2])
        inflammatory_mat_path = os.path.join(sub_dirs, files[3])
        other_mat_path = os.path.join(sub_dirs, files[4])
        

        img = Image.open(img_path)

        mat = scipy.io.loadmat(epithelial_mat_path)
        epithelial_number = processMat(img, mat, "epithelial", epithelial_number)

        mat = scipy.io.loadmat(fibroblast_mat_path)
        fibroblast_number = processMat(img, mat, "fibroblast", fibroblast_number)

        mat = scipy.io.loadmat(inflammatory_mat_path)
        inflammatory_number = processMat(img, mat, "inflammatory", inflammatory_number)

        mat = scipy.io.loadmat(other_mat_path)
        other_number = processMat(img, mat, "others", other_number)

        print(files[0], " done")
        



       