import os
import scipy.io
from scipy import ndimage as ndi
from PIL import Image
from PIL import ImageDraw
from PIL import ImageChops
from PIL import ImageOps
import numpy as np

from skimage.morphology import watershed
from skimage.feature import peak_local_max

#ROOT_PATH = "C:\\Users\\RobNoFlop\\repos\\forschungsprojekt2\\data\\DetectionWatershed"
ROOT_PATH = "C:\\Users\\RobNoFlop\\repos\\forschungsprojekt2\\data\\Detection"
RADIUS = 12

segtype = "watershed"

for sub_dirs, dirs, files in os.walk(ROOT_PATH):
    if sub_dirs is not ROOT_PATH:
        img_path = os.path.join(sub_dirs, files[0])
        # print(img_path)
        img = Image.open(img_path)
        mat_path = os.path.join(sub_dirs, files[1])
        # print(mat_path)
        mat = scipy.io.loadmat(mat_path)

        mask = Image.new(img.mode, img.size, "white")
        draw = ImageDraw.Draw(mask)
        for point in mat['detection']:
            # print(point)
            draw.ellipse((point[0] - RADIUS, point[1] - RADIUS,
                          point[0] + RADIUS, point[1] + RADIUS),
                         fill=(0, 0, 0, 255))

            out = ImageChops.subtract(img, mask)
             
        #out = out.convert('L')

        if segtype is "threshold":
            threshold = 185
            out = out.point(lambda x: 0 if x < threshold and x > 1 else 255)
            out = ImageOps.invert(out)

        if segtype is "watershed":
            outArray = np.asarray(img.convert("L"))
            print(type(outArray), outArray.dtype, outArray.shape)
            distance = ndi.distance_transform_edt(outArray)
            local_maxi = peak_local_max(distance, indices=False, footprint=np.ones((3, 3)))
            markers = ndi.label(local_maxi)[0]
            outWater = watershed(-1 * distance, markers, mask=outArray)
            out = Image.fromarray(outWater)
            out = out.convert("L")

        out.save(os.path.join(sub_dirs, "lableWatershed.png"))
        print(files[0], " done")
        